﻿namespace App
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.start = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.man = new System.Windows.Forms.PictureBox();
            this.timer_ = new System.Windows.Forms.Label();
            this.score = new System.Windows.Forms.Label();
            this.Access = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.man)).BeginInit();
            this.SuspendLayout();
            // 
            // start
            // 
            this.start.BackColor = System.Drawing.Color.White;
            this.start.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.start.Location = new System.Drawing.Point(12, 759);
            this.start.Name = "start";
            this.start.Size = new System.Drawing.Size(78, 31);
            this.start.TabIndex = 0;
            this.start.Text = "Start";
            this.start.UseVisualStyleBackColor = false;
            this.start.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.White;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Location = new System.Drawing.Point(624, 759);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(78, 31);
            this.button2.TabIndex = 1;
            this.button2.Text = "Exit";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // man
            // 
            this.man.BackColor = System.Drawing.Color.Transparent;
            this.man.Image = global::App.Properties.Resources.Man;
            this.man.InitialImage = global::App.Properties.Resources.Man;
            this.man.Location = new System.Drawing.Point(237, 602);
            this.man.Name = "man";
            this.man.Size = new System.Drawing.Size(44, 41);
            this.man.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.man.TabIndex = 2;
            this.man.TabStop = false;
            this.man.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // timer_
            // 
            this.timer_.AutoSize = true;
            this.timer_.Location = new System.Drawing.Point(96, 791);
            this.timer_.Name = "timer_";
            this.timer_.Size = new System.Drawing.Size(13, 13);
            this.timer_.TabIndex = 5;
            this.timer_.Text = "0";
            this.timer_.Click += new System.EventHandler(this.label1_Click);
            // 
            // score
            // 
            this.score.AutoSize = true;
            this.score.Location = new System.Drawing.Point(96, 725);
            this.score.Name = "score";
            this.score.Size = new System.Drawing.Size(13, 13);
            this.score.TabIndex = 6;
            this.score.Text = "0";
            // 
            // Access
            // 
            this.Access.AutoSize = true;
            this.Access.Location = new System.Drawing.Point(96, 759);
            this.Access.Name = "Access";
            this.Access.Size = new System.Drawing.Size(13, 13);
            this.Access.TabIndex = 7;
            this.Access.Text = "0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(237)))), ((int)(((byte)(112)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(714, 813);
            this.Controls.Add(this.Access);
            this.Controls.Add(this.score);
            this.Controls.Add(this.timer_);
            this.Controls.Add(this.man);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.start);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.man)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button start;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.PictureBox man;
        private System.Windows.Forms.Label timer_;
        private System.Windows.Forms.Label score;
        private System.Windows.Forms.Label Access;
    }
}

