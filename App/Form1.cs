﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Threading;


namespace App
{
    public partial class Form1 : Form
    {
        PictureBox pictureBox1 = new PictureBox();
        Random rand = new Random();

        public static int size = 720;
        public static int wall_len = 0;
        public static int Plus_len = 0;
        public static int Negative_len = 0;

        int score_numder = 0;
        bool is_start = false;

        int[] first = { 0, 0 };
        int[,] wall = new int[((size / 60) * 4 + (size / 25)), 2];
        int[,] Plus = new int[(size / 45) + 2, 2];
        int[,] Negative = new int[(size / 45) + 2, 2];
        int time = 0;

        public static int graph_size_ = (((size / 60)) * ((size / 60)));
        int[,] graph_ = new int[graph_size_, graph_size_];

        int[] position = { 1, 1 };
        public static Bitmap flag = new Bitmap(size, size);
        Graphics flagGraphics = Graphics.FromImage(flag);
        Brush Wall_Color = new SolidBrush(Color.FromArgb(113, 183, 234));
        Brush plus_Color = new SolidBrush(Color.FromArgb(255, 77, 125));
        Brush Negative_color = new SolidBrush(Color.FromArgb(96, 96, 96));
        Brush bgcolor = new SolidBrush(Color.FromArgb(0, 0, 0));
        Brush help_color = new SolidBrush(Color.FromArgb(40, 229, 75));

        public void Timer()
        {
            while (true)
            {
                Thread.Sleep(1000);
                time++;
                try
                {
                    timer_.Invoke((Action)delegate
                    {
                        timer_.Text = "Time : " + time;
                    });
                }
                catch (Exception)
                {
                    Application.Exit();
                }

            }
        }

        public void Access_()
        {
            while (true)
            {
                int ok = 0;
                for (int ii = 0; ii < Plus_len; ii++)
                    if (find_Path((Plus[ii, 0] * (size / 60) + Plus[ii, 1]), (position[0] * (size / 60) + position[1])))
                        ok++;
                try
                {
                    Access.Invoke((Action)delegate
                    {
                        Access.Text = "Ative : " + Plus_len + " - Access to : " + ok;
                    });
                }
                catch (Exception)
                {
                }

                Thread.Sleep(100);

            }
        }

        public static List<int> DijkstraAlgorithm(int[,] graph, int sourceNode, int destinationNode)
        {
            var n = graph.GetLength(0);

            var distance = new int[n];
            for (int i = 0; i < n; i++)
            {
                distance[i] = int.MaxValue;
            }

            distance[sourceNode] = 0;

            var used = new bool[n];
            var previous = new int?[n];

            while (true)
            {
                var minDistance = int.MaxValue;
                var minNode = 0;
                for (int i = 0; i < n; i++)
                {
                    if (!used[i] && minDistance > distance[i])
                    {
                        minDistance = distance[i];
                        minNode = i;
                    }
                }

                if (minDistance == int.MaxValue)
                {
                    break;
                }

                used[minNode] = true;

                for (int i = 0; i < n; i++)
                {
                    if (graph[minNode, i] > 0)
                    {
                        var shortestToMinNode = distance[minNode];
                        var distanceToNextNode = graph[minNode, i];

                        var totalDistance = shortestToMinNode + distanceToNextNode;

                        if (totalDistance < distance[i])
                        {
                            distance[i] = totalDistance;
                            previous[i] = minNode;
                        }
                    }
                }
            }

            if (distance[destinationNode] == int.MaxValue)
            {
                return null;
            }

            var path = new LinkedList<int>();
            int? currentNode = destinationNode;
            while (currentNode != null)
            {
                path.AddFirst(currentNode.Value);
                currentNode = previous[currentNode.Value];
            }

            return path.ToList();
        }
        public bool find_Path(int sourceNode, int destinationNode)
        {
            var path = DijkstraAlgorithm(graph_, sourceNode, destinationNode);
            if (path == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }


        public void round_wall()
        {
            pictureBox1.Size = new Size(720, 720);
            this.Controls.Add(pictureBox1);


            flagGraphics.FillRectangle(bgcolor, 0, 0, size, size);
            //y , x
            int[] x_z = { 0, 0 };

            while (x_z[0] < (size / 60))
            {
                flagGraphics.FillRectangle(Wall_Color, 0, (size - (x_z[0] * 60)) - 50, 50, 50);

                wall[wall_len, 0] = x_z[0];
                wall[wall_len, 1] = 0;
                wall_len++;

                flagGraphics.FillRectangle(Wall_Color, (size - size % 60) - 60, (size - (x_z[0] * 60)) - 50, 50, 50);

                wall[wall_len, 0] = x_z[0];
                wall[wall_len, 1] = (size / 60) - 1;
                wall_len++;
                x_z[0] += 1;
            }

            while (x_z[1] < (size / 60))
            {
                flagGraphics.FillRectangle(Wall_Color, ((x_z[1] * 60)), 10 + (size % 60), 50, 50);

                wall[wall_len, 0] = (size / 60) - 1;
                wall[wall_len, 1] = x_z[1];
                wall_len++;

                flagGraphics.FillRectangle(Wall_Color, ((x_z[1] * 60)), size - 50, 50, 50);

                wall[wall_len, 0] = 0;
                wall[wall_len, 1] = x_z[1];
                wall_len++;

                x_z[1] += 1;
            }

            man.Location = new Point(60, size - 110);
            man.Height = 50;
            man.Width = 50;
            pictureBox1.Image = flag;

        }
        public void midel_wall(int how, int num)
        {

            for (int i = 0; i < num; i++)
            {

                int wall_x = rand.Next(1, size / 60 - 1);
                int wall_y = rand.Next(1, size / 60 - 1);
                if (wall_y != position[0] && wall_x != position[1])
                {

                    if (how == 0)
                    {
                        int ok = 0;

                        for (int ii = 0; ii < Plus.GetLength(0); ii++)
                            if (!(Plus[ii, 0] == wall_y && Plus[ii, 1] == wall_x))
                                ok++;
                        if ((ok) == Plus.GetLength(0))
                        {

                            flagGraphics.FillRectangle(Wall_Color, (wall_x * 60), (((size / 60) - wall_y) * 60) - 50, 50, 50);
                            wall[wall_len, 0] = wall_y;
                            wall[wall_len, 1] = wall_x;
                            wall_len++;
                        }
                    }

                    if (how == 1)
                    {


                        if (check(new int[] { wall_y, wall_x }) == 0)
                        {
                            fill_graph();
                            if (find_Path(13, (wall_y * (size / 60) + wall_x)))
                            {
                                flagGraphics.FillRectangle(plus_Color, (wall_x * 60), (((size / 60) - wall_y) * 60) - 50, 50, 50);
                                Plus[Plus_len, 0] = wall_y;
                                Plus[Plus_len, 1] = wall_x;
                                Plus_len++;
                            }
                            else i--;
                        }
                        else i--;
                    }
                }
                else
                    i--;

            }
        }
        public void midel_wall_recover()
        {
            for (int i = 0; i < wall_len; i++)
                flagGraphics.FillRectangle(Wall_Color, (wall[i, 1] * 60), (((size / 60) - wall[i, 0]) * 60) - 50, 50, 50);

            for (int i = 0; i < Plus_len; i++)
                flagGraphics.FillRectangle(plus_Color, (Plus[i, 1] * 60), (((size / 60) - Plus[i, 0]) * 60) - 50, 50, 50);

            for (int i = 0; i < Negative_len; i++)
                flagGraphics.FillRectangle(Negative_color, (Negative[i, 1] * 60), (((size / 60) - Negative[i, 0]) * 60) - 50, 50, 50);
        }

        public int check(int[] pos)
        {

            for (int ii = 0; ii < wall_len; ii++)
                if ((wall[ii, 0] == pos[0] && wall[ii, 1] == pos[1]))
                    return 2;


            for (int ii = 0; ii < Plus_len; ii++)
                if ((Plus[ii, 0] == pos[0] && Plus[ii, 1] == pos[1]))
                    return 3;


            return 0;

        }
        public int[] number_to_xy(int num)
        {
            //y , x
            int[] xy = { 0, 0 };

            for (int i = 1; i <= num; i++)
            {
                if (i % (size / 60) == 0)
                {
                    xy[0]++;
                }
            }

            xy[1] = num - (xy[0] * (size / 60));
            return xy;

        }
        public void fill_graph()
        {
            for (int i = 0; i < graph_.GetLength(0); i++)
            {
                for (int ii = 0; ii < graph_.GetLength(0); ii++)
                {

                    if (check(number_to_xy(i)) == 2)
                    {
                        graph_[i, ii] = 0;
                    }

                    else

                    {
                        if ((ii == i + (size / 60)) || (i + 1 == ii) || (i - 1 == ii) || (ii == i - (size / 60)))
                        {
                            if (check(number_to_xy(ii)) == 2)
                            {
                                graph_[i, ii] = 0;
                            }
                            else
                            {
                                graph_[i, ii] = 1;
                            }
                        }
                        else
                        {
                            graph_[i, ii] = 0;
                        }
                    }
                }
            }
        }

        public void delete(int how, int[] item)
        {

            if (how == 0)
            {
                for (int i = 0; i < Plus.GetLength(0); i++)
                {
                    if (Plus[i, 0] == item[0] && Plus[i, 1] == item[1])
                    {
                        Plus = RemoveRow(Plus, i);
                        Plus_len--;
                    }
                }
            }
            if (how == 1)
            {
                for (int i = 0; i < wall.GetLength(0); i++)
                {
                    if (wall[i, 0] == item[0] && wall[i, 1] == item[1])
                    {
                        wall = RemoveRow(wall, i);
                        wall_len--;
                    }
                }
            }
        }

        public void relode(int size_, int[] pos)
        {
            size = 720;

            wall_len = 0;
            Plus_len = 0;
            Negative_len = 0;


            first = new int[] { 0, 0 };
            wall = new int[((size / 60) * 4 + (size / 25)), 2];
            Plus = new int[(size / 45) + 2, 2];
            Negative = new int[(size / 45) + 2, 2];

            graph_size_ = (((size / 60)) * ((size / 60)));
            graph_ = new int[graph_size_, graph_size_];

            position = new int[] { 1, 1 };
            flag = new Bitmap(size, size);
            flagGraphics = Graphics.FromImage(flag);




            round_wall();
            
            midel_wall(0, (size / 25));
            fill_graph();
            midel_wall(1, (size / 45));

        }
        public Form1()
        {
           
            InitializeComponent();
            round_wall();
            midel_wall(0, (size / 25));
            fill_graph();
            midel_wall(1, (size / 45));

        }

        static T[,] CreateRectangularArray<T>(IList<T[]> arrays)
        {
            // TODO: Validation and special-casing for arrays.Count == 0
            int minorLength = arrays[0].Length;
            T[,] ret = new T[arrays.Count, minorLength];
            for (int i = 0; i < arrays.Count; i++)
            {
                var array = arrays[i];
                if (array.Length != minorLength)
                {
                    throw new ArgumentException
                        ("All arrays must be the same length");
                }
                for (int j = 0; j < minorLength; j++)
                {
                    ret[i, j] = array[j];
                }
            }
            return ret;
        }
        public T[,] RemoveRow<T>(T[,] array2d, int rowToRemove)
        {
            var resultAsList = Enumerable
                .Range(0, array2d.GetLength(0))  // select all the rows available
                .Where(i => i != rowToRemove)    // except for the one we don't want
                .Select(i =>                     // select the results as a string[]
            {
                T[] row = new T[array2d.GetLength(1)];
                for (int column = 0; column < array2d.GetLength(1); column++)
                {
                    row[column] = array2d[i, column];
                }
                return row;
            }).ToList();

            // convert List<string[]> to string[,].
            return CreateRectangularArray(resultAsList); // CreateRectangularArray() can be copied from https://stackoverflow.com/a/9775057
        }

        dynamic Add2d(int[,] input, int many)
        {
            int[,] Out = new int[input.GetLength(0) + many, 2];
            for (int i = 0; i < input.GetLength(0); i++)
            {
                Out[i, 0] = input[i, 0];
                Out[i, 1] = input[i, 1];
            }
            return Out;
        }
        public bool is_posibel(int[] pos)
        {
            fill_graph();
            int ok = 0;
            for (int ii = 0; ii < Plus_len; ii++)
                if (find_Path((Plus[ii, 0] * (size / 60) + Plus[ii, 1]), (pos[0] * (size / 60) + pos[1])))
                    ok++;

            if (ok == Plus_len)
            {
                return true;
            }
            return false;
        }

        public void set_score()
        {
            
            for (int i = 0; i < Negative.GetLength(0); i++)
            {

                if (Negative[i, 0] == position[0] && Negative[i, 1] == position[1])
                {
                    score_numder--;
                    if (score_numder >= 0)
                        score.Text = "score : " + (score_numder).ToString();
                    else
                    {
                        if (MessageBox.Show("Re play ?", "Game over", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            position[0] = 0;
                            position[1] = 1;
                            man.Location = new Point(first[0], first[1]);
                            score_numder = 0;
                            score.Text = "score : " + (score_numder).ToString();
                        }
                        else
                        {
                            if (MessageBox.Show("Are you sure ?", "Exit", MessageBoxButtons.YesNo) == DialogResult.Yes)
                            {
                                Application.Exit();
                            }
                            else
                            {
                                position[0] = 0;
                                position[1] = 1;
                                man.Location = new Point(first[0], first[1]);
                                score_numder = 0;
                                score.Text = "score : " + (score_numder).ToString();
                            }
                        }
                    }
                }
            }


            if (check(position) == 3)
            {
                score_numder = score_numder + 1;
                score.Text = "score : " + (score_numder).ToString();
            }

           

        }

        public void find_near_plus(int[] pos)
        {
            int[] neare = { Plus_len, (size / 60) * (size / 60) };
            for (int i = 0; i < Plus_len; i++)
            {
                var path1 = DijkstraAlgorithm(graph_, (pos[0] * (size / 60) + pos[1]), (Plus[i, 0] * (size / 60) + Plus[i, 1]));
                if (path1 != null)
                    if (path1.Count < neare[1])
                        neare[0] = i;
            }

            var path = DijkstraAlgorithm(graph_, (pos[0] * (size / 60) + pos[1]), (Plus[neare[0], 0] * (size / 60) + Plus[neare[0], 1]));
            if (path != null)
            {
                for (int i = 0; i < path.Count ; i++)
                {
                    
                    flagGraphics.FillRectangle(help_color, (number_to_xy(path[i])[1] * 60), (((size / 60) - number_to_xy(path[i])[0]) * 60) - 50, 50, 50);
                    Thread.Sleep(100);
                    pictureBox1.Image = flag;

                   

                }

               
                midel_wall_recover();
                pictureBox1.Image = flag;



            }

        }
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (is_start)
            {
                if (Plus_len == 0)
                {

                    this.relode(size, position);

                }

                if(keyData == Keys.H)
                {
                    find_near_plus(position);
                }

                //capture up arrow key
                if (keyData == Keys.Up)
                {

                    if (check(new int[] { position[0] + 1, position[1] }) != 2)
                    {

                        int x = man.Location.X;
                        int y = man.Location.Y - 60;

                        int[] new_position = new int[] { position[0] + 1, position[1] };
                        if (check(position) == 3)
                        {
                            delete(0, position);

                            wall = Add2d(wall, 1);
                            wall[wall_len, 0] = position[0];
                            wall[wall_len, 1] = position[1];
                            wall_len++;

                            int[] new_position_top = new int[] { new_position[0] - 1, new_position[1] };
                            int[] new_position_buttom = new int[] { position[0] + 1, new_position[1] };
                            int[] new_position_right = new int[] { new_position[0], new_position[1] + 1 };
                            int[] new_position_left = new int[] { new_position[0], new_position[1] - 1 };

                            int count = 0;
                            if (is_posibel(new_position_top)) count++;
                            if (is_posibel(new_position_buttom)) count++;
                            if (is_posibel(new_position_right)) count++;
                            if (is_posibel(new_position_left)) count++;

                            if (count < 2)
                            {
                                delete(1, position);
                                Negative[Negative_len, 0] = position[0];
                                Negative[Negative_len, 1] = position[1];
                                Negative_len++;
                            }

                            flagGraphics.FillRectangle(bgcolor, 0, 0, size, size);
                            midel_wall_recover();
                        }

                        position[0]++;
                        man.Location = new Point(x, y);



                        set_score();
                    }

                    return true;
                }

                //capture down arrow key
                if (keyData == Keys.Down)
                {

                    if (check(new int[] { position[0] - 1, position[1] }) != 2)
                    {

                        int x = man.Location.X;
                        int y = man.Location.Y + 60;


                        int[] new_position = new int[] { position[0] - 1, position[1] };

                        if (check(position) == 3)
                        {
                            delete(0, position);

                            wall = Add2d(wall, 1);
                            wall[wall_len, 0] = position[0];
                            wall[wall_len, 1] = position[1];
                            wall_len++;

                            int[] new_position_top = new int[] { new_position[0] - 1, new_position[1] };
                            int[] new_position_buttom = new int[] { position[0] + 1, new_position[1] };
                            int[] new_position_right = new int[] { new_position[0], new_position[1] + 1 };
                            int[] new_position_left = new int[] { new_position[0], new_position[1] - 1 };

                            int count = 0;
                            if (is_posibel(new_position_top)) count++;
                            if (is_posibel(new_position_buttom)) count++;
                            if (is_posibel(new_position_right)) count++;
                            if (is_posibel(new_position_left)) count++;

                            if (count < 2)
                            {
                                delete(1, position);
                                Negative[Negative_len, 0] = position[0];
                                Negative[Negative_len, 1] = position[1];
                                Negative_len++;
                            }
                            flagGraphics.FillRectangle(bgcolor, 0, 0, size, size);
                            midel_wall_recover();
                        }

                        man.Location = new Point(x, y);

                        position[0]--;
                        set_score();
                    }

                    return true;
                }

                //capture left arrow key
                if (keyData == Keys.Left)
                {


                    if (check(new int[] { position[0], position[1] - 1 }) != 2)
                    {

                        int x = man.Location.X - 60;
                        int y = man.Location.Y;

                        int[] new_position = new int[] { position[0], position[1] - 1 };

                        if (check(position) == 3)
                        {
                            delete(0, position);

                            wall = Add2d(wall, 1);
                            wall[wall_len, 0] = position[0];
                            wall[wall_len, 1] = position[1];
                            wall_len++;

                            int[] new_position_top = new int[] { new_position[0] - 1, new_position[1] };
                            int[] new_position_buttom = new int[] { position[0] + 1, new_position[1] };
                            int[] new_position_right = new int[] { new_position[0], new_position[1] + 1 };
                            int[] new_position_left = new int[] { new_position[0], new_position[1] - 1 };

                            int count = 0;
                            if (is_posibel(new_position_top)) count++;
                            if (is_posibel(new_position_buttom)) count++;
                            if (is_posibel(new_position_right)) count++;
                            if (is_posibel(new_position_left)) count++;

                            if (count < 2)
                            {
                                delete(1, position);
                                Negative[Negative_len, 0] = position[0];
                                Negative[Negative_len, 1] = position[1];
                                Negative_len++;
                            }

                            flagGraphics.FillRectangle(bgcolor, 0, 0, size, size);
                            midel_wall_recover();
                        }

                        man.Location = new Point(x, y);

                        position[1]--;
                        set_score();
                    }

                    return true;
                }

                //capture right arrow key
                if (keyData == Keys.Right)
                {
                    if (check(new int[] { position[0], position[1] + 1 }) != 2)
                    {

                        int x = man.Location.X + 60;
                        int y = man.Location.Y;

                        int[] new_position = new int[] { position[0], position[1] + 1 };
                        if (check(position) == 3)
                        {
                            delete(0, position);

                            wall = Add2d(wall, 1);
                            wall[wall_len, 0] = position[0];
                            wall[wall_len, 1] = position[1];
                            wall_len++;

                            int[] new_position_top = new int[] { new_position[0] - 1, new_position[1] };
                            int[] new_position_buttom = new int[] { position[0] + 1, new_position[1] };
                            int[] new_position_right = new int[] { new_position[0], new_position[1] + 1 };
                            int[] new_position_left = new int[] { new_position[0], new_position[1] - 1 };

                            int count = 0;
                            if (is_posibel(new_position_top)) count++;
                            if (is_posibel(new_position_buttom)) count++;
                            if (is_posibel(new_position_right)) count++;
                            if (is_posibel(new_position_left)) count++;

                            if (count < 2)
                            {
                                delete(1, position);
                                Negative[Negative_len, 0] = position[0];
                                Negative[Negative_len, 1] = position[1];
                                Negative_len++;
                            }

                            flagGraphics.FillRectangle(bgcolor, 0, 0, size, size);
                            midel_wall_recover();
                        }

                        man.Location = new Point(x, y);

                        position[1]++;
                        set_score();
                    }
                }
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            is_start = true;
            score.Text = "score : " + (score_numder).ToString();
            
            ThreadStart childref = new ThreadStart(Timer);
            Thread childThread = new Thread(childref);
            childThread.Start();

            ThreadStart childref_Access = new ThreadStart(Access_);
            Thread childThread_Access = new Thread(childref_Access);
            childThread_Access.Start();
            
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //  score.Text = "Welcome to the nonsense game";
            int x = man.Location.X;
            int y = man.Location.Y;
            first[0] = x;
            first[1] = y;


        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure ?", "Exit", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void score_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
